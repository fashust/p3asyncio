# -*- coding: utf-8 -*- #
"""
    async io hello world
"""
import asyncio


__author__ = 'fashust'
__email__ = 'fashust.nefor@gmail.com'


def hello(loop):
    """
        echo hello world
    """
    print('hello world!')
    loop.call_later(2, hello, loop)


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.call_soon(hello, loop)
    loop.run_forever()