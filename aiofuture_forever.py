# -*- coding: utf-8 -*- #
"""
    asyncio future with run_forever
"""
from asyncio import get_event_loop, Task, Future, sleep, coroutine


__author__ = 'fashust'
__email__ = 'fashust.nefor@gmail.com'


@coroutine
def simulate(future):
    """
        simulate long running process
    """
    yield from sleep(5)
    future.set_result('some long running result')


def simulate_callback(future):
    """
        long running process callback
    """
    print(future.result())
    loop.stop()


loop = get_event_loop()
future = Future()
Task(simulate(future))
future.add_done_callback(simulate_callback)
try:
    loop.run_forever()
except Exception as err:
    print(err)
finally:
    loop.close()