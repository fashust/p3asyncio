# -*- coding: utf-8 -*- #
"""
    asyncio signals handling example
"""
import asyncio
import functools
import os
import signal


__author__ = 'fashust'
__email__ = 'fashust.nefor@gmail.com'


def sig_exit(signal_name):
    """
        handle exit signal
    """
    print('got signal: {}'.format(signal_name))
    loop.stop()

loop = asyncio.get_event_loop()
for signal_name in ('SIGINT', 'SIGTERM'):
    loop.add_signal_handler(
        getattr(signal, signal_name),
        functools.partial(sig_exit, signal_name)
    )

loop.run_forever()