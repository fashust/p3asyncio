# -*- coding: utf-8 -*- #
"""
    multiply client handler
"""
from asyncio import get_event_loop, Task, coroutine, start_server


__author__ = 'fashust'
__email__ = 'fashust.nefor@gmail.com'


clients = {}


def accept_client(client_reader, client_writer):
    """
        accept client connection
    """
    task = Task(handle_client(client_reader, client_writer))
    clients[task] = (client_reader, client_writer)
    clients_count = len([clients.keys()])
    print('Handle clients: {}'.format(clients_count))

    def done(t):
        """
            clear task
        """
        del(clients[t])
        print('client disconnected')

    task.add_done_callback(done)


@coroutine
def handle_client(client_reader, client_writer):
    """
        handle client
    """
    while True:
        data = yield from client_reader.readline()
        print('data: {}'.format(data))
        client_writer.write(data.upper())

loop = get_event_loop()
s = start_server(accept_client, host='localhost', port=8000)
server = loop.run_until_complete(s)
loop.run_forever()