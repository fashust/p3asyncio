# -*- coding: utf-8 -*- #
"""
    asyncio parallels tasks executions
"""
from asyncio import get_event_loop, Task, coroutine, sleep, wait


__author__ = 'fashust'
__email__ = 'fashust.nefor@gmail.com'


@coroutine
def factorial(name, number):
    """
        calculate factorial for number
    """
    f = 1
    for i in range(2, number+1):
        print('Task: {}, compute factorial({})'.format(name, i))
        yield from sleep(1)
        f *= i
    print('Task: {}, factorial({}) = {}'.format(name, number, f))


tasks = [
    Task(factorial('A', 2)),
    Task(factorial('B', 3)),
    Task(factorial('C', 4)),
]

loop = get_event_loop()
loop.run_until_complete(wait(tasks))
loop.close()