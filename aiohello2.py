# -*- coding: utf-8 -*- #
"""
    hello world with coroutine
"""
import asyncio


__author__ = 'fashust'
__email__ = 'fashust.nefor@gmail.com'


@asyncio.coroutine
def say_hello():
    """
        say hello
    """
    while True:
        print('hello world')
        yield from asyncio.sleep(2)

loop = asyncio.get_event_loop()
loop.run_until_complete(say_hello())