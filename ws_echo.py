# -*- coding: utf-8 -*-
"""
    asyncio websocket echo server
"""
from _sha1 import sha1
import asyncio
from base64 import encodebytes


LOOP = asyncio.get_event_loop()


@asyncio.coroutine
def convert_headers(data, STATUS):
    """
        convert headers to dict
    """
    headers = {}
    end_index = data.find(b'\r\n\r\n')
    data = data[:end_index].split(b'\r\n')
    method, path, protocol = [x.decode('utf-8') for x in data[0].split(b' ')]
    for item in data[1:]:
        key, val = [x.decode('utf-8') for x in item.split(b': ', 1)]
        headers[key] = val
    headers["Location"] = "ws://" + headers["Host"] + path
    STATUS.append(True)
    return headers


@asyncio.coroutine
def handhsake(headers):
    """
        create handshake response
    """
    r = b''
    key = headers["Sec-WebSocket-Key"]
    r += b'HTTP/1.1 101 Switching Protocols\r\n'
    r += b'Upgrade: WebSocket\r\n'
    r += b'Connection: Upgrade\r\n'
    key = response_key(key)
    r += b'Sec-WebSocket-Accept: ' + key + b'\r\n'
    r += 'Sec-WebSocket-Protocol: {}\r\n'.format(
        headers["Sec-WebSocket-Protocol"]
    ).encode('utf-8') if headers.get("Sec-WebSocket-Protocol", None) else b''
    return r


def response_key(key):
    """
        ws key
    """
    guid = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11"
    combined = key + guid
    hashed = sha1(combined.encode('ascii')).digest()
    return encodebytes(hashed)


@asyncio.coroutine
def handle_connections(reader, writer):
    """
        handle connections
    """
    STATUS = []
    chunck = 4096
    request = {}
    while True:
        # data = yield from reader.read(n=chunck)
        data = yield from reader.read(n=chunck)
        print(len(data))
        if data:
            if not STATUS:
                print('raw data', data)
                request = yield from convert_headers(data, STATUS)
                print('request', request)
                response = yield from handhsake(request)
                print('response', response)
                writer.write(response)
            else:
                print(data)
                writer.write(data)


def main():
    """
        main
    """
    gen_server = asyncio.start_server(
        handle_connections, port=8001, host='0.0.0.0'
    )
    server = LOOP.run_until_complete(gen_server)
    LOOP.run_forever()


if __name__ == '__main__':
    main()