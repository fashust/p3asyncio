# -*- coding: utf-8 -*- #
"""
    asyncio stream
"""
from _sha1 import sha1
import asyncio
from base64 import encodebytes
import struct
import array


__author__ = 'fashust'
__email__ = 'fashust.nefor@gmail.com'


def response_key(key):
    """
        ws key
    """
    guid = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11"
    combined = key + guid
    hashed = sha1(combined.encode('ascii')).digest()
    return encodebytes(hashed)


def handshake(headers):
    """
        ws handshake
    """
    end_index = headers.find(b'\r\n\r\n')
    headers = headers[:end_index].split(b'\r\n')
    method, path, protocol = [x.decode('utf-8') for x in headers[0].split(b' ')]
    headers_dct = {}
    for item in headers[1:]:
        key, val = [x.decode('utf-8') for x in item.split(b': ', 1)]
        headers_dct[key] = val
    headers_dct["Location"] = "ws://" + headers_dct["Host"] + path
    r = b''
    key = headers_dct["Sec-WebSocket-Key"]
    r += b'HTTP/1.1 101 Switching Protocols\r\n'
    r += b'Upgrade: WebSocket\r\n'
    r += b'Connection: Upgrade\r\n'
    key = response_key(key)
    r += b'Sec-WebSocket-Accept: ' + key + b'\r\n'
    r += 'Sec-WebSocket-Protocol: {}\r\n'.format(
        headers_dct["Sec-WebSocket-Protocol"]
    ).encode('utf-8') if headers_dct.get("Sec-WebSocket-Protocol", None) else b''
    return r


def parse_frame(frame):
    """
        parse frame
    """
    buff = frame[:]
    payload_start = 2
    b = buff[0]
    fin = buff[0] & 0b10000000
    rsv = buff[0] & 0b01110000
    opcode = buff[0] & 0b00001111
    mask_flag = buff[1] & 0b10000000
    length = buff[1] & 0b01111111
    if len(frame) < payload_start + 4:
        return
    elif length == 126:
        length, = struct.unpack(">H", frame[2:4])
        payload_start += 2
    elif length == 127:
        length, = struct.unpack(">I", frame[2:6])
        print('length', length)
        payload_start += 4
    mask = None
    if mask_flag:
        mask = [ord(chr(b)) for b in frame[payload_start:payload_start + 4]]
        payload_start += 4
    if len(buff) < payload_start + length:
        return
    payload = frame[payload_start:payload_start + length]
    if mask_flag:
        # unmasked = [
        #     mask[i % 4] ^ ord(chr(b)) for b, i in zip(
        #         payload, range(len(payload))
        #     )
        # ]
        unmasked = [
            mask[i % 4] ^ ord(chr(b)) for b, i in zip(
                payload, range(len(payload))
            )
        ]
        print(unmasked, len(unmasked))
        # payload = ''.join([chr(c) for c in unmasked])
        payload = unmasked
    return payload



@asyncio.coroutine
def connection_handler(reader, writer):
    """
        connection handler
    """
    peername = writer.get_extra_info('peername')
    print(peername)
    data = b''
    connected = False
    chunck_size = 4089
    while True:
        if not connected:
            line = yield from reader.readline()
            if line == b'\r\n':
                data = b''.join([data, line])
                writer.write(handshake(data))
                connected = True
                data = b''
                continue
            else:
                data = b''.join([data, line])
                continue
        else:
            frame = yield from reader.read(n=chunck_size)
            data += frame
            result = parse_frame(data)
            if result:
                data = b''
                frame = b''
                print(b''.join(result).decode())


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    server_gen = asyncio.start_server(
        connection_handler, host='0.0.0.0', port=8001
    )
    server = loop.run_until_complete(server_gen)
    loop.run_forever()