import asyncio
import aiohttp


@asyncio.coroutine
def load(url, sem):
    """
        load url
    """
    with(yield from sem):
        response = yield from aiohttp.request('GET', url)
        return (yield from response.read_and_close(decode=True))


@asyncio.coroutine
def get_page(page, sem):
    """
        get page
    """
    url = 'http://google.com/#q={}'.format(page)
    text = yield from load(url, sem)
    print('URL {}: TEXT: {}'.format(url, text[:20]))


def main():
    """
        main
    """
    loop = asyncio.get_event_loop()
    sem = asyncio.Semaphore(10)
    requests = [get_page(x, sem) for x in range(1000)]
    f = asyncio.wait(requests)
    loop.run_until_complete(f)

if __name__ == '__main__':
    main()
