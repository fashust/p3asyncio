# -*- coding: utf-8 -*- #
"""
    asyncio future
"""
from asyncio import sleep, get_event_loop, coroutine, Future, Task


__author__ = 'fashust'
__email__ = 'fashust.nefor@gmail.com'


@coroutine
def slow_operation(future):
    """
        simulate slow operation
    """
    yield from sleep(10)
    future.set_result('Some result after slow operation')


loop = get_event_loop()
future = Future()
Task(slow_operation(future))
loop.run_until_complete(future)
print('some else')
print(future.result())
loop.close()