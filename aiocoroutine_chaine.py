# -*- coding: utf-8 -*- #
"""
    asyncio coroutine chain
"""
import asyncio


__author__ = 'fashust'
__email__ = 'fashust.nefor@gmail.com'


@asyncio.coroutine
def mul(x, y):
    """
        multiply x and y
    """
    print('Multiply {} * {}'.format(x, y))
    yield from asyncio.sleep(1)
    return x * y


@asyncio.coroutine
def print_mul(x, y):
    """
        print result of x * y
    """
    result = yield from mul(x, y)
    print('result of {} * {} = {}'.format(x, y, result))


loop = asyncio.get_event_loop()
loop.run_until_complete(print_mul(5, 3))
loop.close()